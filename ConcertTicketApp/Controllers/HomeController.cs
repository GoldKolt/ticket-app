﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ConcertTicketApp.Models;
using ConcertTicketApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ConcertTicketApp.Models.ViewModels;
using static ConcertTicketApp.Utils.SortOrder;
using ConcertTicketApp.Services;
using ConcertTicketApp.Utils;
using static ConcertTicketApp.Utils.ConcertType;

namespace ConcertTicketApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitOfWork unitOfWork;
        private readonly IEmailSender emailSender;

        public HomeController(UserManager<ApplicationUser> userManager, IUnitOfWork unitOfWork, IEmailSender emailSender)
        {
            _userManager = userManager;
            this.unitOfWork = unitOfWork;
            this.emailSender = emailSender;
        }

        // GET: Concert
        public async Task<IActionResult> Index(int page = 1, int pageSize = 10, SortConcert sortOrder = SortConcert.NameAscending)
        {
            ViewData["Action"] = "Index";
            InitializeViewData(sortOrder);
            PaginatedList<Concert> concerts = await unitOfWork.ConcertRepository.GetAll(sortOrder: sortOrder, pageIndex: page, pageSize: pageSize);
            return View(concerts);
        }

        public async Task<IActionResult> SearchByType(Types? searchValue, Types currentFilter, int page = 1, int pageSize = 10, SortConcert sortOrder = SortConcert.NameAscending)
        {
            if (searchValue.HasValue)
            {
                page = 1;
            }
            else
            {
                searchValue = currentFilter;
            }
            ViewData["Action"] = "SearchByType";
            InitializeViewData(sortOrder, searchValue.ToString());
            PaginatedList<Concert> concerts = await unitOfWork.ConcertRepository.GetAll(filter: concert => concert.Discriminator.ToLower().Contains(searchValue.ToString().ToLower()), sortOrder: sortOrder, pageIndex: page, pageSize: pageSize);
            return View("Index", concerts);
        }

        public async Task<IActionResult> SearchByName(string searchValue, string currentFilter, int page = 1, int pageSize = 10, SortConcert sortOrder = SortConcert.NameAscending)
        {
            ViewData["Action"] = "SearchByName";
            if (searchValue != null)
            {
                page = 1;
            }
            else
            {
                searchValue = currentFilter ?? "";
            }
            InitializeViewData(sortOrder, searchValue);
            PaginatedList<Concert> concerts = await unitOfWork.ConcertRepository.GetAll(filter: concert => concert.Name.ToLower().Contains(searchValue.ToLower()), sortOrder: sortOrder, pageIndex: page, pageSize: pageSize);
            return View("Index", concerts);
        }

        // GET: Concert/Details/5
        public async Task<IActionResult> Details(int id, string alert = null)
        {
            Concert concert = await unitOfWork.ConcertRepository.Find(id);
            ViewData["BookingMessage"] = alert;
            return View(concert);
        }

        // GET: Concert/Create
        [Authorize(Roles = "admin")]
        public IActionResult Create()
        {
            ViewData["Types"] = Enum.GetNames(typeof(Types));
            return View();
        }

        // POST: Concert/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Create(FullConcertViewModel concertViewModel)
        {
            ViewData["Types"] = Enum.GetNames(typeof(Types));
            ViewData["CurrentType"] = concertViewModel.Discriminator;
            if (ModelState.IsValid)
            {
                Concert concert = Activator.CreateInstance(GetConcertType(concertViewModel.Discriminator)) as Concert;
                concert.MapFromViewModel(concertViewModel);
                await unitOfWork.ConcertRepository.Add(concert);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Concert/Edit/5
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            ViewData["Types"] = Enum.GetNames(typeof(Types));
            if (id != null)
            {
                var concert = await unitOfWork.ConcertRepository.Find(id.Value);
                if (concert != null)
                {
                    FullConcertViewModel viewModel = concert.MapToViewModel();
                    return View(viewModel);
                }
            }
            return NotFound();
        }

        // POST: Concert/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(int id, FullConcertViewModel concertViewModel)
        {
            var concert = await unitOfWork.ConcertRepository.Find(id);
            if (concert == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                concert.MapFromViewModel(concertViewModel);
                await unitOfWork.ConcertRepository.Update(concert);
                return RedirectToAction("Index");
            }
            return View(concertViewModel);
        }

        // GET: Concert/Delete/5
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id != null)
            {
                var concert = await unitOfWork.ConcertRepository.Find(id.Value);
                if (concert != null)
                {
                    return View(concert);
                }
            }
            return NotFound();
        }

        // POST: Concert/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var concert = await unitOfWork.ConcertRepository.Find(id);
            if (concert == null)
            {
                return NotFound();
            }
            await unitOfWork.ConcertRepository.Remove(concert);
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        public async Task<IActionResult> BookTicket(int concertId)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user == null)
            {
                return NotFound();
            }
            Concert concert = await unitOfWork.ConcertRepository.Find(concertId);
            bool bookingResult = await unitOfWork.ConcertRepository.Book(concert);
            if (bookingResult)
            {
                await unitOfWork.TicketRepository.Add(concertId, user.Id);
                EmailSenderModel email = new EmailSenderModel()
                {
                    To = user.Email,
                    Subject = "Booking ticket",
                    Body = string.Concat($"<h2>You booked a ticket for a concert</h2><h4>", concert.Name, "</h4><h4>Address:", concert.Address, "</h4><h4>Date:", concert.Date, "</h4><h4>Price of ticket:", concert.Price, "</h4>")
                };
                await emailSender.SendEmailAsync(email);
                return RedirectToAction("Details", new { id = concertId, alert = "Booked sucksessfully" });
            }
            return RedirectToAction("Details", new { id = concertId, alert = "Sorry, booking a ticket was failed" });
        }

        private void InitializeViewData(SortConcert sortOrder, string currentFilter = "")
        {
            ViewData["CurrentFilter"] = currentFilter;
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParam"] = (sortOrder == SortConcert.NameAscending) ? "NameDescending" : "";
            ViewData["DateSortParam"] = (sortOrder == SortConcert.DateAscending) ? "DateDescending" : "DateAscending";
            ViewData["PriceSortParam"] = (sortOrder == SortConcert.PriceAscending) ? "PriceDescending" : "PriceAscending";
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

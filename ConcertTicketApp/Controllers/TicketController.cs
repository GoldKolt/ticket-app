﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertTicketApp.Data;
using ConcertTicketApp.Models;
using ConcertTicketApp.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ConcertTicketApp.Controllers
{
    [Authorize]
    public class TicketController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitOfWork unitOfWork;

        public TicketController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Index(string alert = null, int page = 1, int pageSize = 20)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            PaginatedList<Ticket> tickets = await unitOfWork.TicketRepository.GetAll(t => t.ApplicationUserId.Equals(user.Id), includeProperties: "Concert", pageIndex: page, pageSize: pageSize);
            ViewData["Alert"] = alert;
            return View(tickets);
        }

        public async Task<IActionResult> UnBookTicket(int id)
        {
            Ticket ticket = await unitOfWork.TicketRepository.Find(id, includeProperties: "Concert");
            if (ticket == null)
            {
                return RedirectToAction("Index", new { alert = "Error while unbooking" });
            }
            await unitOfWork.ConcertRepository.Unbook(ticket.Concert);
            await unitOfWork.TicketRepository.Remove(ticket);
            return RedirectToAction("Index", new { alert = "Unbooked ticket successed" });
        }
    }
}
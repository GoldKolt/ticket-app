﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ConcertTicketApp.Models;

namespace ConcertTicketApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Concert> Concerts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ClassicMusicConcert>().HasBaseType<Concert>();
            builder.Entity<PartyConcert>().HasBaseType<Concert>();
            builder.Entity<OpenAirConcert>().HasBaseType<Concert>();
            base.OnModelCreating(builder);
        }
    }
}

﻿using ConcertTicketApp.Models;
using ConcertTicketApp.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static ConcertTicketApp.Utils.SortOrder;

namespace ConcertTicketApp.Data.EFRepositories
{
    public class ConcertRepository : IConcertRepository
    {
        private ApplicationDbContext context;

        public ConcertRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task Add(Concert concert)
        {
            await context.Concerts.AddAsync(concert);
            await Save();
        }

        public async Task<Concert> Find(int id, Expression<Func<Concert, bool>> filter = null, string includeProperties = "")
        {
            IQueryable<Concert> query = context.Concerts;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return await query.FirstOrDefaultAsync(c=>c.Id == id);
        }

        public async Task<PaginatedList<Concert>> GetAll(Expression<Func<Concert, bool>> filter = null, SortConcert sortOrder = SortConcert.NameAscending, string includeProperties = "", int pageIndex = 1, int pageSize = 10)
        {
            IQueryable<Concert> query = context.Concerts;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            query = await Sort(query, sortOrder);
            return await PaginatedList<Concert>.CreateAsync(query, pageIndex, pageSize);
        }

        public async Task Remove(int id)
        {
            Concert entityToDelete = await Find(id);
            await Remove(entityToDelete);
        }

        public async Task Remove(Concert concert)
        {
            context.Concerts.Remove(concert);
            await Save();
        }

        public async Task Update(Concert concert)
        {
            context.Concerts.Update(concert);
            await Save();
        }

        public async Task<bool> Book(Concert concert)
        {
            if (concert.Tickets <= 0)
            {
                return false;
            }
            concert.Tickets--;
            context.Concerts.Update(concert);
            await Save();
            return true;
        }

        public async Task<bool> Unbook(Concert concert)
        {
            concert.Tickets++;
            context.Concerts.Update(concert);
            await Save();
            return true;
        }

        private Task<IQueryable<Concert>> Sort(IQueryable<Concert> query, SortConcert sortOrder)
        {
            switch (sortOrder)
            {
                case SortConcert.DateAscending:
                    query = query.OrderBy(s => s.Date);
                    break;
                case SortConcert.DateDescending:
                    query = query.OrderByDescending(s => s.Date);
                    break;
                case SortConcert.PriceAscending:
                    query = query.OrderBy(s => s.Price);
                    break;
                case SortConcert.PriceDescending:
                    query = query.OrderByDescending(s => s.Price);
                    break;
                case SortConcert.NameDescending:
                    query = query.OrderByDescending(s => s.Name);
                    break;
                default:
                    query = query.OrderBy(s => s.Name);
                    break;
            }
            return Task.FromResult(query);
        }

        private async Task Save()
        {
            await context.SaveChangesAsync();
        }
    }
}

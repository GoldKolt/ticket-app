﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ConcertTicketApp.Models;
using ConcertTicketApp.Utils;
using Microsoft.EntityFrameworkCore;

namespace ConcertTicketApp.Data.EFRepositories
{
    public class TicketRepository : ITicketRepository
    {
        private ApplicationDbContext context;

        public TicketRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task Add(int concertId, string userId)
        {
            Ticket ticket = new Ticket()
            {
                ConcertId = concertId,
                ApplicationUserId = userId
            };
            await context.Tickets.AddAsync(ticket);
            await Save();
        }

        public async Task<Ticket> Find(int id, Expression<Func<Ticket, bool>> filter = null, string includeProperties = "")
        {
            IQueryable<Ticket> query = context.Tickets;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return await query.FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<PaginatedList<Ticket>> GetAll(Expression<Func<Ticket, bool>> filter = null, Func<IQueryable<Ticket>, IOrderedQueryable<Ticket>> orderBy = null, string includeProperties = "", int pageIndex = 1, int pageSize = 10)
        {
            IQueryable<Ticket> query = context.Tickets;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return await PaginatedList<Ticket>.CreateAsync(query, pageIndex, pageSize);
        }

        public async Task Remove(int id)
        {
            Ticket entityToDelete = await Find(id);
            await Remove(entityToDelete);
        }

        public async Task Remove(Ticket ticket)
        {
            context.Tickets.Remove(ticket);
            await Save();
        }

        public async Task UpdateAsync(Ticket ticket)
        {
            context.Tickets.Update(ticket);
            await Save();
        }

        private async Task Save()
        {
            await context.SaveChangesAsync();
        }
    }
}

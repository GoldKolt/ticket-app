﻿using ConcertTicketApp.Data.EFRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertTicketApp.Data
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext context;
        private ITicketRepository ticketRepository;
        private IConcertRepository concertRepository;

        public ITicketRepository TicketRepository
        {
            get
            {
                if (ticketRepository == null)
                {
                    ticketRepository = new TicketRepository(context);
                }
                return ticketRepository;
            }
        }

        public IConcertRepository ConcertRepository
        {
            get
            {
                if (concertRepository == null)
                {
                    concertRepository = new ConcertRepository(context);
                }
                return concertRepository;
            }
        }

        public EFUnitOfWork(IConfiguration config)
        {
            DbContextOptionsBuilder<ApplicationDbContext> options = new DbContextOptionsBuilder<ApplicationDbContext>();
            options.UseSqlServer(config.GetConnectionString("DefaultConnection"));
            context = new ApplicationDbContext(options.Options);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

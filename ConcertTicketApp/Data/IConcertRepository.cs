﻿using ConcertTicketApp.Models;
using ConcertTicketApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static ConcertTicketApp.Utils.SortOrder;

namespace ConcertTicketApp.Data
{
    public interface IConcertRepository
    {
        Task<Concert> Find(
            int id,
            Expression<Func<Concert, bool>> filter = null,
            string includeProperties = "");
        Task<PaginatedList<Concert>> GetAll(
            Expression<Func<Concert, bool>> filter = null, 
            SortConcert sortOrder = SortConcert.NameAscending, 
            string includeProperties = "",
            int pageIndex = 1,
            int pageSize = 10);
        Task Add(Concert concert);
        Task Update(Concert concert);
        Task Remove(int id);
        Task Remove(Concert concert);
        Task<bool> Book(Concert concert);
        Task<bool> Unbook(Concert concert);
    }
}

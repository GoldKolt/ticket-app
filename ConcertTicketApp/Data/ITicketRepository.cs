﻿using ConcertTicketApp.Models;
using ConcertTicketApp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ConcertTicketApp.Data
{
    public interface ITicketRepository
    {
        Task<Ticket> Find(
            int id, 
            Expression<Func<Ticket, bool>> filter = null, 
            string includeProperties = "");
        Task<PaginatedList<Ticket>> GetAll(
            Expression<Func<Ticket, bool>> filter = null,
            Func<IQueryable<Ticket>, IOrderedQueryable<Ticket>> orderBy = null,
            string includeProperties = "",
            int pageIndex = 1,
            int pageSize = 10);
        Task Add(int concertId, string userId);
        Task UpdateAsync(Ticket ticket);
        Task Remove(int id);
        Task Remove(Ticket ticket);
    }
}

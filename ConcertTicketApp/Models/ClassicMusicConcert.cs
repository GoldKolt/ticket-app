﻿using ConcertTicketApp.Models.ViewModels;

namespace ConcertTicketApp.Models
{
    public class ClassicMusicConcert : Concert
    {
        public string VoiceType { get; set; }
        public string CompositorName { get; set; }

        public override void MapFromViewModel(FullConcertViewModel viewModel)
        {
            base.MapFromViewModel(viewModel);
            VoiceType = viewModel.VoiceType;
            CompositorName = viewModel.CompositorName;
        }

        public override FullConcertViewModel MapToViewModel()
        {
            var result = base.MapToViewModel();
            result.CompositorName = CompositorName;
            result.VoiceType = VoiceType;
            return result;
        }
    }
}

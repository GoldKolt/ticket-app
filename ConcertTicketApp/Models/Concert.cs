﻿using ConcertTicketApp.Models.ViewModels;
using System;
using System.ComponentModel.DataAnnotations;
using ConcertTicketApp.Utils;

namespace ConcertTicketApp.Models
{
    public abstract class Concert
    {
        public int Id { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        [Range(0, 9999999999)]
        public int Tickets { get; set; }
        [Required]
        [Range(0.0, 9999999999.99)]
        public float Price { get; set; }
        public string Discriminator { get; set; }

        public virtual void MapFromViewModel(FullConcertViewModel viewModel)
        {
            Id = viewModel.Id;
            Address = viewModel.Address;
            Date = viewModel.Date;
            Name = viewModel.Name;
            Tickets = viewModel.Tickets;
            Price = float.Parse(viewModel.Price.Replace(',', '.'));
        }

        public virtual FullConcertViewModel MapToViewModel()
        {
            var result = new FullConcertViewModel()
            {
                Address = Address,
                Date = Date,
                Name = Name,
                Tickets = Tickets,
                Price = Price.ToString(),
                Id = Id,
                Discriminator = ConcertType.GetConcertType(Discriminator)
            };
            result.Price = result.Price.Replace('.', ',');
            return result;
        }
        

    }
}

﻿using ConcertTicketApp.Models.ViewModels;

namespace ConcertTicketApp.Models
{
    public class OpenAirConcert : Concert
    {
        public string HeadLiner { get; set; }
        public string HowToGet { get; set; }

        public override void MapFromViewModel(FullConcertViewModel viewModel)
        {
            base.MapFromViewModel(viewModel);
            HeadLiner = viewModel.HeadLiner;
            HowToGet = viewModel.HowToGet;
        }

        public override FullConcertViewModel MapToViewModel()
        {
            var result = base.MapToViewModel();
            result.HowToGet = HowToGet;
            result.HeadLiner = HeadLiner;
            return result;
        }
    }
}

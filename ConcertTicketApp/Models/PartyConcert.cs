﻿using ConcertTicketApp.Models.ViewModels;

namespace ConcertTicketApp.Models
{
    public class PartyConcert : Concert
    {
        public int Year { get; set; }

        public override void MapFromViewModel(FullConcertViewModel viewModel)
        {
            base.MapFromViewModel(viewModel);
            Year = viewModel.Year;
        }

        public override FullConcertViewModel MapToViewModel()
        {
            var result = base.MapToViewModel();
            result.Year = Year;
            return result;
        }
    }
}

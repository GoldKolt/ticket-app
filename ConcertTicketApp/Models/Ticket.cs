﻿namespace ConcertTicketApp.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public string ApplicationUserId { get; set; }
        public Concert Concert { get; set; }
        public int ConcertId { get; set; }
    }
}

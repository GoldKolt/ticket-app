﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static ConcertTicketApp.Utils.ConcertType;

namespace ConcertTicketApp.Models.ViewModels
{
    public class FullConcertViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        [Range(0, 9999999999)]
        public int Tickets { get; set; }
        [Required]
        [RegularExpression("[0-9]{1,9}([,][0-9]{1,2})?", ErrorMessage = "The value is not valid")]
        public string Price { get; set; }
        [Required]
        public Types Discriminator { get; set; }
        public string VoiceType { get; set; }
        public string CompositorName { get; set; }
        public string HeadLiner { get; set; }
        public string HowToGet { get; set; }
        public int Year { get; set; }
    }
}

﻿using ConcertTicketApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ConcertTicketApp.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailSender : IEmailSender
    {
        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Task.CompletedTask;
        }

        public async Task SendEmailAsync(EmailSenderModel model)
        {
            MailAddress from = new MailAddress("goldlocalhostkolt@gmail.com");
            MailAddress to = new MailAddress(model.To);
            MailMessage message = new MailMessage(from, to)
            {
                Subject = model.Subject,
                Body = model.Body,
                IsBodyHtml = true
            };
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("goldlocalhostkolt@gmail.com", "LoCaLHoST13579"),
                EnableSsl = true
            };
            await smtp.SendMailAsync(message);
        }
    }
}

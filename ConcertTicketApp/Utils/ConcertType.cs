﻿using ConcertTicketApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertTicketApp.Utils
{
    public static class ConcertType
    {
        public enum Types { ClassicMusicConcert, PartyConcert, OpenAirConcert }

        public static Types GetConcertType(string type)
        {
            Types result = Types.ClassicMusicConcert;
            switch (type)
            {
                case "ClassicMusicConcert":
                    result = Types.ClassicMusicConcert;
                    break;
                case "OpenAirConcert":
                    result = Types.OpenAirConcert;
                    break;
                case "PartyConcert":
                    result = Types.PartyConcert;
                    break;
            }
            return result;
        }

        public static Type GetConcertType(Types type)
        {
            Type result = null;
            switch (type)
            {
                case Types.ClassicMusicConcert:
                    result = typeof(ClassicMusicConcert);
                    break;
                case Types.OpenAirConcert:
                    result = typeof(OpenAirConcert);
                    break;
                case Types.PartyConcert:
                    result = typeof(PartyConcert);
                    break;
            }
            return result;
        }
    }
}

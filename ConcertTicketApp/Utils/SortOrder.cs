﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertTicketApp.Utils
{
    public class SortOrder
    {
        public enum SortConcert { NameAscending, NameDescending, DateDescending, DateAscending, PriceDescending, PriceAscending }
    }
}
